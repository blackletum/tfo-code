//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef VIEWPORT_PANEL_NAMES_H
#define VIEWPORT_PANEL_NAMES_H
#ifdef _WIN32
#pragma once
#endif

// default panel name definitions
#define PANEL_ALL			"all"		// all current panels
#define PANEL_ACTIVE		"active"	// current active panel

#endif // VIEWPORT_PANEL_NAMES_H