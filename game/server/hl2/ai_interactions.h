//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//

//==================================================
// Definition for all AI interactions
//==================================================

#ifndef	AI_INTERACTIONS_H
#define	AI_INTERACTIONS_H

#ifdef _WIN32
#pragma once
#endif

//Combine
extern int	g_interactionCombineBash;

//ScriptedTarget
extern int  g_interactionScriptedTarget;

// Zombie
// Melee attack will land in one second or so.
extern int	g_interactionZombieMeleeWarning;

#endif	//AI_INTERACTIONS_H